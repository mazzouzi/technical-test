package com.mazzouzi.core.database.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.mazzouzi.core.database.entity.AlbumEntity.Companion.TABLE_NAME

@Entity(tableName = TABLE_NAME)
data class AlbumEntity(
    @PrimaryKey
    @ColumnInfo(name = ID) val id: Long,
    @ColumnInfo(name = ALBUM_ID) val albumId: Long,
    @ColumnInfo(name = TITLE) val title: String,
    @ColumnInfo(name = URL) val url: String,
    @ColumnInfo(name = THUMBNAIL_URL) val thumbnailUrl: String,
) {
    companion object {
        const val TABLE_NAME = "album"
        const val ID = "id"
        const val ALBUM_ID = "album_id"
        const val TITLE = "title"
        const val URL = "url"
        const val THUMBNAIL_URL = "thumbnail_url"
    }
}