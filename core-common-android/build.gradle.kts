import com.mazzouzi.build.logic.Deps

plugins {
    id ("com.android.library")
    id ("org.jetbrains.kotlin.android")
    id ("kotlin-kapt")
    id("com.mazzouzi.dependency")
}

android {
    compileSdk = 32

    defaultConfig {
        minSdk = 24
        targetSdk = 32

        testInstrumentationRunner = "androidx.test.runner.AndroidJUnitRunner"
        consumerProguardFiles("consumer-rules.pro")
    }

    buildTypes {
        release {
            isMinifyEnabled = false
            proguardFiles(getDefaultProguardFile("proguard-android-optimize.txt"), "proguard-rules.pro")
        }
    }

    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_1_8
        targetCompatibility  = JavaVersion.VERSION_1_8
    }

    kotlinOptions {
        jvmTarget = "1.8"
    }

    buildFeatures {
        viewBinding = true
    }
}

dependencies {
    implementation(Deps.core_ktx)
    implementation(Deps.material)
    implementation(Deps.recyclerview)
    implementation(Deps.paging)

    implementation(Deps.glide)
    kapt(Deps.glide_compiler)
    implementation ("com.github.bumptech.glide:okhttp3-integration:4.0.0"){
        exclude(module = "glide-parent")
    }

    implementation(Deps.okhttp)
    implementation(Deps.okhttp_logging_interceptor)
}