package com.mazzouzi.core.network.helper

import com.mazzouzi.core.network.response.ApiResponse
import retrofit2.Response

class NetworkHelperImpl : NetworkHelper {

    override suspend fun <T> callRemote(
        call: suspend () -> Response<T>,
        predicate: ((t: T) -> Boolean)?
    ): ApiResponse<T> = try {
        val response = call()
        val body = response.body()
        if (
            response.isSuccessful &&
            body != null &&
            (predicate == null || predicate(body))
        ) {
            ApiResponse.Success(body)
        } else {
            ApiResponse.Error(null, response.code())
        }
    } catch (exception: Exception) {
        ApiResponse.Error(exception, -1)
    }
}