package com.mazzouzi.build.logic

import com.mazzouzi.build.logic.Version.APP_COMPAT_VERSION
import com.mazzouzi.build.logic.Version.CONSTRAINT_LAYOUT_VERSION
import com.mazzouzi.build.logic.Version.CORE_KTX_VERSION
import com.mazzouzi.build.logic.Version.COROUTINE_VERSION
import com.mazzouzi.build.logic.Version.FRAGMENT_KTX_VERSION
import com.mazzouzi.build.logic.Version.GLIDE_VERSION
import com.mazzouzi.build.logic.Version.HILT_VERSION
import com.mazzouzi.build.logic.Version.LIFECYCLE_RUNTIME_KTX_VERSION
import com.mazzouzi.build.logic.Version.MATERIAL_VERSION
import com.mazzouzi.build.logic.Version.MOSHI_VERSION
import com.mazzouzi.build.logic.Version.OK_HTTP_VERSION
import com.mazzouzi.build.logic.Version.PAGING_VERSION
import com.mazzouzi.build.logic.Version.RECYCLER_VERSION
import com.mazzouzi.build.logic.Version.RETROFIT_VERSION
import com.mazzouzi.build.logic.Version.ROOM_VERSION
import com.mazzouzi.build.logic.Version.SWIPE_REFRESH_VERSION
import com.mazzouzi.build.logic.Version.TIMBER_VERSION
import org.gradle.api.Plugin
import org.gradle.api.Project

class DependencyPlugin : Plugin<Project> {

    override fun apply(target: Project) {
        // no-op
    }
}

object Version {
    // Framework
    const val COROUTINE_VERSION = "1.6.3"
    const val HILT_VERSION = "2.42"
    const val GLIDE_VERSION = "4.13.2"
    const val MATERIAL_VERSION = "1.6.1"
    const val RECYCLER_VERSION = "1.2.1"
    const val LIFECYCLE_RUNTIME_KTX_VERSION = "2.5.0"
    const val CONSTRAINT_LAYOUT_VERSION = "2.1.4"
    const val FRAGMENT_KTX_VERSION= "1.5.0"
    const val OK_HTTP_VERSION = "4.10.0"
    const val MOSHI_VERSION = "1.13.0"
    const val RETROFIT_VERSION = "2.6.4"
    const val ROOM_VERSION = "2.4.2"
    const val APP_COMPAT_VERSION = "1.4.2"
    const val PAGING_VERSION = "3.0.0"
    const val CORE_KTX_VERSION = "1.8.0"
    const val SWIPE_REFRESH_VERSION = "1.1.0"
    const val TIMBER_VERSION = "4.7.1"

    // Testing
    const val JUNIT_VERSION = "5.8.1"
    const val ARCH_CORE_TESTING_VERSION = "2.1.0"
    const val TEST_CORE_KTX = "1.4.0"
    const val ESPRESSO_VERSION = "3.4.0"
    const val TURBINE_VERSION = "0.8.0"
    const val MOCKITO_VERSION = "4.6.1"
    const val MOCKITO_KOTLIN_VERSION = "4.0.0"
    const val COROUTINE_TEST = "1.6.4"
}

object Deps {
    val coroutine = "org.jetbrains.kotlinx:kotlinx-coroutines-core:$COROUTINE_VERSION"
    val hilt = "com.google.dagger:hilt-android:$HILT_VERSION"
    val hilt_core = "com.google.dagger:hilt-core:$HILT_VERSION"
    val hilt_compiler = "com.google.dagger:hilt-compiler:$HILT_VERSION"
    val glide = "com.github.bumptech.glide:glide:$GLIDE_VERSION"
    val glide_compiler = "com.github.bumptech.glide:compiler:$GLIDE_VERSION"
    val lifecycle_runtime_ktx = "androidx.lifecycle:lifecycle-runtime-ktx:$LIFECYCLE_RUNTIME_KTX_VERSION"
    val recyclerview = "androidx.recyclerview:recyclerview:$RECYCLER_VERSION"
    val material = "com.google.android.material:material:$MATERIAL_VERSION"
    val fragment_ktx = "androidx.fragment:fragment-ktx:$FRAGMENT_KTX_VERSION"
    val constraint_layout = "androidx.constraintlayout:constraintlayout:$CONSTRAINT_LAYOUT_VERSION"
    val moshi = "com.squareup.moshi:moshi:$MOSHI_VERSION"
    val moshi_codegen = "com.squareup.moshi:moshi-kotlin-codegen:$MOSHI_VERSION"
    val retrofit = "com.squareup.retrofit2:retrofit:$RETROFIT_VERSION"
    val retrofit_moshi_converter = "com.squareup.retrofit2:converter-moshi:$RETROFIT_VERSION"
    val okhttp = "com.squareup.okhttp3:okhttp:$OK_HTTP_VERSION"
    val okhttp_logging_interceptor = "com.squareup.okhttp3:logging-interceptor:$OK_HTTP_VERSION"
    val room_runtime = "androidx.room:room-runtime:$ROOM_VERSION"
    val room_ktx = "androidx.room:room-ktx:$ROOM_VERSION"
    val room_compiler = "androidx.room:room-compiler:$ROOM_VERSION"
    val app_compat = "androidx.appcompat:appcompat:$APP_COMPAT_VERSION"
    val paging = "androidx.paging:paging-runtime-ktx:$PAGING_VERSION"
    val paging_common = "androidx.paging:paging-common-ktx:$PAGING_VERSION"
    val core_ktx = "androidx.core:core-ktx:$CORE_KTX_VERSION"
    val swipeRefresh = "androidx.swiperefreshlayout:swiperefreshlayout:$SWIPE_REFRESH_VERSION"
    val timber = "com.jakewharton.timber:timber:$TIMBER_VERSION"
}