package com.mazzouzi.core.domain.repository

import androidx.paging.PagingData
import com.mazzouzi.core.domain.model.Album
import kotlinx.coroutines.flow.Flow

interface AlbumRepository {

    fun getAlbums(): Flow<PagingData<Album>>
}