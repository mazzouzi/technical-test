package com.mazzouzi.core.network.di

import com.mazzouzi.core.network.datasource.AlbumRemoteDataSource
import com.mazzouzi.core.network.datasource.AlbumRemoteDataSourceImpl
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(value = [SingletonComponent::class])
abstract class RemoteDataSourceModule {

    @Binds
    @Singleton
    abstract fun providesAlbumRemoteDataSource(
        albumRemoteDataSourceImpl: AlbumRemoteDataSourceImpl
    ): AlbumRemoteDataSource
}