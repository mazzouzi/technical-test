import com.mazzouzi.build.logic.Deps

plugins {
    id ("com.android.library")
    id ("org.jetbrains.kotlin.android")
    id ("kotlin-kapt")
    id ("dagger.hilt.android.plugin")
    id ("de.mannodermaus.android-junit5")
    id ("com.mazzouzi.dependency")
    id ("com.mazzouzi.unit.testing")
}

android {
    compileSdk = 32

    defaultConfig {
        minSdk = 24
        targetSdk = 32

        testInstrumentationRunner = "androidx.test.runner.AndroidJUnitRunner"
        consumerProguardFiles("consumer-rules.pro")
    }

    buildTypes {
        release {
            isMinifyEnabled = false
            proguardFiles(getDefaultProguardFile("proguard-android-optimize.txt"), "proguard-rules.pro")
        }
    }
    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_1_8
        targetCompatibility  = JavaVersion.VERSION_1_8
    }
    kotlinOptions {
        jvmTarget = "1.8"
    }
    buildFeatures {
        viewBinding = true
    }
}

dependencies {

    implementation(project(":core-domain"))
    api(project(":core-common-android"))

    implementation(Deps.material)
    implementation(Deps.recyclerview)
    implementation(Deps.swipeRefresh)
    implementation(Deps.lifecycle_runtime_ktx)
    implementation(Deps.fragment_ktx)
    implementation(Deps.coroutine)
    implementation(Deps.paging)

    implementation(Deps.hilt)
    kapt(Deps.hilt_compiler)

    implementation(Deps.glide)
    kapt(Deps.glide_compiler)

    implementation(Deps.timber)
}