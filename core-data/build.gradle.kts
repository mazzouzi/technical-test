import com.mazzouzi.build.logic.Deps

plugins {
    id ("com.android.library")
    id ("org.jetbrains.kotlin.android")
    id ("kotlin-kapt")
    id ("dagger.hilt.android.plugin")
    id ("de.mannodermaus.android-junit5")
    id ("com.mazzouzi.dependency")
    id ("com.mazzouzi.unit.testing")
}

android {
    compileSdk = 32

    defaultConfig {
        minSdk = 24
        targetSdk = 32

        testInstrumentationRunner = "androidx.test.runner.AndroidJUnitRunner"
        consumerProguardFiles("consumer-rules.pro")
    }

    buildTypes {
        release {
            isMinifyEnabled = false
            proguardFiles(getDefaultProguardFile("proguard-android-optimize.txt"), "proguard-rules.pro")
        }
    }
    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_1_8
        targetCompatibility  = JavaVersion.VERSION_1_8
    }
    kotlinOptions {
        jvmTarget = "1.8"
    }
}

dependencies {

    implementation(project(":core-domain"))
    implementation(project(":core-network"))
    implementation(project(":core-database"))

    implementation(Deps.coroutine)

    implementation(Deps.retrofit)
    implementation(Deps.retrofit_moshi_converter)

    implementation(Deps.hilt)
    kapt(Deps.hilt_compiler)

    implementation(Deps.paging)
}