package com.mazzouzi.core.data.mapper

import com.mazzouzi.core.database.entity.AlbumEntity
import com.mazzouzi.core.domain.model.Album
import com.mazzouzi.core.network.model.AlbumNetwork
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test

class AlbumMapperTest {

    @Test
    fun `map AlbumNetwork to AlbumEntity`() {
        Assertions.assertEquals(albumEntity, albumNetwork.toAlbumEntity())
    }

    @Test
    fun `map AlbumEntity to Album`() {
        Assertions.assertEquals(album, albumEntity.toAlbum())
    }

    companion object {
        val album: List<Album> = listOf(
            Album(1, 2, "TITLE 1", "http://url1", "http://image/url1"),
            Album(2, 1, "TITLE 2", "http://url2", "http://image/url2"),
            Album(3, 7, "TITLE 3", "http://url3", "http://image/url3")
        )

        val albumNetwork: List<AlbumNetwork> = listOf(
            AlbumNetwork(1, 2, "TITLE 1", "http://url1", "http://image/url1"),
            AlbumNetwork(2, 1, "TITLE 2", "http://url2", "http://image/url2"),
            AlbumNetwork(3, 7, "TITLE 3", "http://url3", "http://image/url3")
        )

        val albumEntity: List<AlbumEntity> = listOf(
            AlbumEntity(1, 2, "TITLE 1", "http://url1", "http://image/url1"),
            AlbumEntity(2, 1, "TITLE 2", "http://url2", "http://image/url2"),
            AlbumEntity(3, 7, "TITLE 3", "http://url3", "http://image/url3")
        )
    }
}