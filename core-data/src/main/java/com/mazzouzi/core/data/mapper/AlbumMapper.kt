package com.mazzouzi.core.data.mapper

import com.mazzouzi.core.database.entity.AlbumEntity
import com.mazzouzi.core.domain.model.Album
import com.mazzouzi.core.network.model.AlbumNetwork

fun AlbumNetwork.toAlbumEntity(): AlbumEntity = AlbumEntity(
    id = id,
    albumId = albumId,
    title = title,
    url = url,
    thumbnailUrl = thumbnailUrl
)

fun List<AlbumNetwork>.toAlbumEntity() = map { it.toAlbumEntity() }

fun AlbumEntity.toAlbum() = Album(
    id = id,
    albumId = albumId,
    title = title,
    url = url,
    thumbnailUrl = thumbnailUrl
)

fun List<AlbumEntity>.toAlbum() = map { it.toAlbum() }