plugins {
    `kotlin-dsl`
}

java {
    sourceCompatibility = JavaVersion.VERSION_1_8
    targetCompatibility = JavaVersion.VERSION_1_8
}

repositories {
    mavenCentral()
}

gradlePlugin {
    plugins {
        register("android-dependency") {
            id = "com.mazzouzi.dependency"
            implementationClass = "com.mazzouzi.build.logic.DependencyPlugin"
        }

        register("android-instrumented-testing") {
            id = "com.mazzouzi.instrumented.testing"
            implementationClass = "com.mazzouzi.build.logic.InstrumentedTestingPlugin"
        }

        register("kotlin-unit-testing") {
            id = "com.mazzouzi.unit.testing"
            implementationClass = "com.mazzouzi.build.logic.UnitTestingPlugin"
        }
    }
}