<h2> Architecture / Stack </h2>

* Clean Archi / MVVM for scalability and better separation of concerns
* Coroutine and Flow are used to connect feature with app logic
* Dependency injection using Hilt
* Paging 3 to handle list pagination
* Glide to load image urls
* Retrofit / OkHttp / Moshi to handle network request
* Room for the database

* JUnit Jupiter / Mockito / Turbine for unit testing
* Jetpack component instrumented test

<h2> Modularisation </h2>

- Modularised app with flat structure and a graph height of 2 :

![Screenshot_2022-07-15_at_16.03.19](/uploads/d95d04cbef08d31ea1611e750961e287/Screenshot_2022-07-15_at_16.03.19.png)

**:app** -> simply contains MainActivity class and is responsible for loading the first fragment

**:core-commmon-android** -> contains Android framework related classes such as adapters or interfaces

**:core-common-kotlin** -> contains kotlin utility classes to wrap network/local data and so on

**:core-data** -> contains repository and data logic (see "AlbumPagingSource.kt"). Single source of truth : ROOM database

**:core-database** -> contains all database related classes (dao, entity, database)

**:core-network** -> contains network apis and models

**:core-domain** -> the most abstract module which handle app business logic

**:feature-album** -> the only feature of this app which is responsible for displaying a list of albums retrieved from the network and paginated through the ROOM database using Paging component

<h2> Gradle (WIP) </h2>

Using Gradle Composite builds for better performance. See "build-logic" module.<br/> 
3 plugins are available : 
1) "DependencyPlugin" to handle library dependencies
2) "InstrumentedTestingPlugin" to handle instrumented tests only (applied in "core-database")
3) "UnitTestingPlugin" to handle unit tests only (applied in "album-feature" and "core-data")

<h2> Testing </h2>

**Instrumented test** -> see "AlbumDaoTest.kt" <br/>
**Unit test** -> see "PagingSourceTest.kt" and "AlbumViewModelTest.kt"

<h2> Features </h2>

5 use cases have been implemented (from left to right respectively): <br/>
- Loading state (when we first launch the app)
- Error state (when network request fails and there's no data available in the DB)
- Content state (we're displaying the first albums)
- Offline support
- Pagination with error management 

![Screenshot_2022-07-15_at_18.18.25](/uploads/f7fa17234be217f6a4ea9abc6a7443ed/Screenshot_2022-07-15_at_18.18.25.png)







