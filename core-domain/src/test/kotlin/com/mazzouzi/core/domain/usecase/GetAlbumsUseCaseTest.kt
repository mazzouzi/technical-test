package com.mazzouzi.core.domain.usecase

import androidx.paging.PagingData
import androidx.paging.map
import app.cash.turbine.test
import com.mazzouzi.core.domain.model.Album
import com.mazzouzi.core.domain.repository.AlbumRepository
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.test.runTest
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.mockito.Mockito

@OptIn(ExperimentalCoroutinesApi::class)
internal class GetAlbumsUseCaseTest {

    private lateinit var useCase: GetAlbumsUseCase
    private val repository = Mockito.mock(AlbumRepository::class.java)

    @BeforeEach
    fun setUp() {
        useCase = GetAlbumsUseCase(repository)
    }

    @Test
    fun getAlbums() = runTest {
        Mockito.`when`(repository.getAlbums()).thenReturn(
            flow { emit(PagingData.from(albums)) }
        )

        useCase.getAlbums().test {
            val albumPagingData: PagingData<Album> = awaitItem()
            albumPagingData.map { album ->
                assertTrue(albums.contains(album))
            }
            awaitComplete()
        }
    }

    companion object {
        val albums = listOf(
            Album(
                id = 1,
                albumId = 1,
                title = "et quisquam aspernatur",
                url = "https://via.placeholder.com/600/cc12f5",
                thumbnailUrl = "https://via.placeholder.com/150/cc12f5"
            ),
            Album(
                id = 2,
                albumId = 1,
                title = "reprehenderit est deserunt velit ipsam",
                url = "https://via.placeholder.com/600/771796",
                thumbnailUrl = "https://via.placeholder.com/150/771796"
            )
        )
    }
}