package com.mazzouzi.core.network.datasource

import com.mazzouzi.core.network.response.ApiResponse
import com.mazzouzi.core.network.model.AlbumNetwork

interface AlbumRemoteDataSource {

    suspend fun getAlbums(): ApiResponse<List<AlbumNetwork>>
}