package com.mazzouzi.core.database.dao

import androidx.room.*
import com.mazzouzi.core.database.entity.AlbumEntity

@Dao
interface AlbumDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertOrReplaceAlbumEntities(albums: List<AlbumEntity>)

    @Query(value = "SELECT * FROM ${AlbumEntity.TABLE_NAME} LIMIT :limit OFFSET :offset" )
    suspend fun getAlbumEntitiesByPage(limit: Int, offset: Int): List<AlbumEntity>

    @Query(value = "SELECT * FROM ${AlbumEntity.TABLE_NAME}")
    suspend fun getAlbumEntities(): List<AlbumEntity>

    @Delete
    suspend fun deleteAlbums(toDelete: List<AlbumEntity>)
}