package com.mazzouzi.core.network.response

sealed interface ApiResponse<out T> {
    data class Success<T>(val data: T) : ApiResponse<T>
    data class Error(val exception: Throwable? = null, val errorCode: Int) : ApiResponse<Nothing>
}