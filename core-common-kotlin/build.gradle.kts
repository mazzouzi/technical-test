import com.mazzouzi.build.logic.Deps

plugins {
    id ("java-library")
    id ("org.jetbrains.kotlin.jvm")
    id("kotlin-kapt")
    id("com.mazzouzi.dependency")
}

java {
    sourceCompatibility = JavaVersion.VERSION_1_8
    targetCompatibility = JavaVersion.VERSION_1_8
}

dependencies {
    implementation(Deps.coroutine)

    implementation(Deps.hilt_core)
    kapt(Deps.hilt_compiler)
}
