package com.mazzouzi.core.data.repository

import androidx.paging.PagingData
import com.mazzouzi.core.database.entity.AlbumEntity
import com.mazzouzi.core.domain.model.Album
import com.mazzouzi.core.network.model.AlbumNetwork
import com.mazzouzi.core.network.response.ApiResponse
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow

class FakeAlbumPagerFactoryImpl : AlbumPagerFactory {

    override fun getAlbums(
        pageSize: Int,
        prefetchDistance: Int,
        enablePlaceholders: Boolean,
        getLocalData: suspend (Int, Int) -> List<AlbumEntity>,
        getRemoteData: suspend () -> ApiResponse<List<AlbumNetwork>>,
        updateLocalData: suspend (List<AlbumNetwork>) -> Unit,
        needToFetchRemoteAlbums: () -> Boolean,
        resetRemoteSyncTime: () -> Unit,
        mapToAlbum: (List<AlbumEntity>) -> List<Album>
    ): Flow<PagingData<Album>> = flow {
        needToFetchRemoteAlbums()

        getRemoteData()

        resetRemoteSyncTime()
        updateLocalData(albumNetwork)

        val albumEntities = getLocalData(30, 0)

        val albums = mapToAlbum(albumEntities)

        emit(PagingData.from(albums))
    }

    companion object {
        val albumNetwork = listOf(
            AlbumNetwork(
                id = 1,
                albumId = 1,
                title = "et quisquam aspernatur",
                url = "https://via.placeholder.com/600/cc12f5",
                thumbnailUrl = "https://via.placeholder.com/150/cc12f5"
            ),
            AlbumNetwork(
                id = 2,
                albumId = 1,
                title = "reprehenderit est deserunt velit ipsam",
                url = "https://via.placeholder.com/600/771796",
                thumbnailUrl = "https://via.placeholder.com/150/771796"
            )
        )
    }
}