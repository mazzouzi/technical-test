package com.mazzouzi.core.data.repository

import androidx.paging.PagingSource
import androidx.paging.PagingState
import com.mazzouzi.core.network.response.ApiResponse

internal class GenericPagingSource<NETWORK, ENTITY, DOMAIN : Any>(
    private inline val getLocalData: suspend (Int, Int) -> List<ENTITY>,
    private inline val getRemoteData: suspend () -> ApiResponse<List<NETWORK>>,
    private inline val updateLocalData: suspend (List<NETWORK>) -> Unit,
    private inline val needToFetchRemoteData: () -> Boolean,
    private inline val resetRemoteSyncTime: () -> Unit,
    private inline val mapToDomain: (List<ENTITY>) -> List<DOMAIN>
) : PagingSource<Int, DOMAIN>() {

    override suspend fun load(params: LoadParams<Int>): LoadResult<Int, DOMAIN> =
        try {
            val page = params.key ?: 0

            if (page == 0 && needToFetchRemoteData()) {
                when (val apiResponse = getRemoteData()) {
                    is ApiResponse.Success -> {
                        resetRemoteSyncTime()
                        updateLocalData(apiResponse.data)
                    }
                    else -> {
                        // do nothing, offline fallback
                    }
                }
            }

            val entities = getLocalData(params.loadSize, page * params.loadSize)

            when {
                page == 0 && entities.isEmpty() -> {
                    // if network request fails and there's no local data then return an error
                    LoadResult.Error(IllegalStateException())
                }
                else -> {
                    LoadResult.Page(
                        data = mapToDomain(entities),
                        prevKey = null, // no backward paging
                        nextKey = if (entities.isEmpty()) null else page + 1
                    )
                }
            }
        } catch (e: Exception) {
            LoadResult.Error(e)
        }

    override fun getRefreshKey(state: PagingState<Int, DOMAIN>): Int? =
        state.anchorPosition?.let { anchorPosition ->
            val anchorPage = state.closestPageToPosition(anchorPosition)
            anchorPage?.prevKey?.plus(1) ?: anchorPage?.nextKey?.minus(1)
        }
}