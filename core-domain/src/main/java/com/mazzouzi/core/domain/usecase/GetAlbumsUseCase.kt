package com.mazzouzi.core.domain.usecase

import androidx.paging.PagingData
import com.mazzouzi.core.domain.model.Album
import com.mazzouzi.core.domain.repository.AlbumRepository
import kotlinx.coroutines.flow.Flow

class GetAlbumsUseCase(
    private val repository: AlbumRepository
) {
    fun getAlbums(): Flow<PagingData<Album>> = repository.getAlbums()
}