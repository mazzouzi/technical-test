package com.mazzouzi.build.logic

import com.mazzouzi.build.logic.Version.COROUTINE_TEST
import com.mazzouzi.build.logic.Version.JUNIT_VERSION
import com.mazzouzi.build.logic.Version.MOCKITO_KOTLIN_VERSION
import com.mazzouzi.build.logic.Version.MOCKITO_VERSION
import com.mazzouzi.build.logic.Version.TURBINE_VERSION
import org.gradle.api.Plugin
import org.gradle.api.Project
import org.gradle.kotlin.dsl.dependencies

class UnitTestingPlugin : Plugin<Project> {

    override fun apply(target: Project) {
        with(target) {

            dependencies {
                add("testImplementation", "org.junit.jupiter:junit-jupiter-api:$JUNIT_VERSION")
                add("testImplementation", "org.junit.jupiter:junit-jupiter-engine:$JUNIT_VERSION")
                add("testImplementation", "org.junit.jupiter:junit-jupiter-params:$JUNIT_VERSION")

                add("testImplementation", "org.mockito:mockito-core:$MOCKITO_VERSION")
                add("testImplementation", "org.mockito:mockito-inline:$MOCKITO_VERSION")
                add("testImplementation", "org.mockito.kotlin:mockito-kotlin:$MOCKITO_KOTLIN_VERSION")

                add("testImplementation", "org.jetbrains.kotlinx:kotlinx-coroutines-test:$COROUTINE_TEST")
                add("testImplementation", "app.cash.turbine:turbine:$TURBINE_VERSION")
            }
        }
    }
}