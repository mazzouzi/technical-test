package com.mazzouzi.core.database

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.room.Room
import androidx.test.core.app.ApplicationProvider
import com.mazzouzi.core.database.entity.AlbumEntity
import kotlinx.coroutines.runBlocking
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertTrue

class AlbumDaoTest {

    @get:Rule
    var instantExecutorRule = InstantTaskExecutorRule()

    private lateinit var database: AppDatabase

    @Before
    fun initDb() {
        database = Room.inMemoryDatabaseBuilder(
            ApplicationProvider.getApplicationContext(),
            AppDatabase::class.java
        ).allowMainThreadQueries().build()
    }

    @After
    fun closeDb() = database.close()

    @Test
    fun insertAlbumsThenFetchFirstPage() = runBlocking {
        val album1 = AlbumEntity(
            id = 1,
            albumId = 1,
            title = "accusamus beatae ad facilis cum similique qui sunt",
            url = "https://via.placeholder.com/600/92c952",
            thumbnailUrl = "https://via.placeholder.com/150/92c952"
        )
        val album2 = AlbumEntity(
            id = 2,
            albumId = 1,
            title = "reprehenderit est deserunt velit ipsam",
            url = "https://via.placeholder.com/600/771796",
            thumbnailUrl = "https://via.placeholder.com/150/771796"
        )

        database.albumDao().insertOrReplaceAlbumEntities(listOf(album1, album2))

        val albumEntities = database.albumDao().getAlbumEntitiesByPage(15, 0)

        assertEquals(albumEntities.size, 2)
        assertEquals(albumEntities[0], album1)
        assertEquals(albumEntities[1], album2)
    }

    @Test
    fun insertAlbumsThenFetchThemAll() = runBlocking {
        val album1 = AlbumEntity(
            id = 1,
            albumId = 1,
            title = "accusamus beatae ad facilis cum similique qui sunt",
            url = "https://via.placeholder.com/600/92c952",
            thumbnailUrl = "https://via.placeholder.com/150/92c952"
        )
        val album2 = AlbumEntity(
            id = 2,
            albumId = 1,
            title = "reprehenderit est deserunt velit ipsam",
            url = "https://via.placeholder.com/600/771796",
            thumbnailUrl = "https://via.placeholder.com/150/771796"
        )

        val album3 = AlbumEntity(
            id = 3,
            albumId = 3,
            title = "reprehenderit est deserunt velit ipsam",
            url = "https://via.placeholder.com/600/771796",
            thumbnailUrl = "https://via.placeholder.com/150/771796"
        )

        database.albumDao().insertOrReplaceAlbumEntities(
            listOf(album1, album2, album3)
        )

        val albumEntities = database.albumDao().getAlbumEntities()

        assertEquals(albumEntities.size, 3)
        assertEquals(albumEntities[0], album1)
        assertEquals(albumEntities[1], album2)
        assertEquals(albumEntities[2], album3)
    }

    @Test
    fun insertAlbumsThenReplaceAndFetchThem() = runBlocking {
        val album1 = AlbumEntity(
            id = 1,
            albumId = 1,
            title = "accusamus beatae ad facilis cum similique qui sunt",
            url = "https://via.placeholder.com/600/92c952",
            thumbnailUrl = "https://via.placeholder.com/150/92c952"
        )
        val album2 = AlbumEntity(
            id = 2,
            albumId = 1,
            title = "reprehenderit est deserunt velit ipsam",
            url = "https://via.placeholder.com/600/771796",
            thumbnailUrl = "https://via.placeholder.com/150/771796"
        )
        val album1Replacement = AlbumEntity(
            id = 1,
            albumId = 1,
            title = "officia porro iure quia iusto qui ipsa ut modi",
            url = "https://via.placeholder.com/600/24f355",
            thumbnailUrl = "https://via.placeholder.com/150/24f355"
        )

        database.albumDao().insertOrReplaceAlbumEntities(listOf(album1, album2))
        database.albumDao().insertOrReplaceAlbumEntities(listOf(album1Replacement))

        val albumEntities = database.albumDao().getAlbumEntities()

        assertEquals(albumEntities.size, 2)
        assertEquals(albumEntities[0], album1Replacement)
        assertEquals(albumEntities[1], album2)
    }

    @Test
    fun insertAlbumsThenDeleteThem() = runBlocking {
        val album1 = AlbumEntity(
            id = 1,
            albumId = 1,
            title = "accusamus beatae ad facilis cum similique qui sunt",
            url = "https://via.placeholder.com/600/92c952",
            thumbnailUrl = "https://via.placeholder.com/150/92c952"
        )
        val album2 = AlbumEntity(
            id = 2,
            albumId = 1,
            title = "reprehenderit est deserunt velit ipsam",
            url = "https://via.placeholder.com/600/771796",
            thumbnailUrl = "https://via.placeholder.com/150/771796"
        )

        database.albumDao().insertOrReplaceAlbumEntities(listOf(album1, album2))
        database.albumDao().deleteAlbums(listOf(album1, album2))

        val albumEntities = database.albumDao().getAlbumEntities()
        assertTrue(albumEntities.isEmpty())
    }
}
