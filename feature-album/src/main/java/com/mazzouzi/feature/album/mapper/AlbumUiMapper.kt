package com.mazzouzi.feature.album.mapper

import com.mazzouzi.core.domain.model.Album
import com.mazzouzi.feature.album.AlbumUi

fun Album.toAlbumUi() = AlbumUi(
    id = id,
    albumId = albumId,
    title = title,
    url = url,
    thumbnailUrl = thumbnailUrl
)