package com.mazzouzi.feature.album.di

import com.mazzouzi.core.domain.repository.AlbumRepository
import com.mazzouzi.core.domain.usecase.GetAlbumsUseCase
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ViewModelComponent
import dagger.hilt.android.scopes.ViewModelScoped

@Module
@InstallIn(value = [ViewModelComponent::class])
object AlbumPresenterModule {

    @ViewModelScoped
    @Provides
    fun providesGetAlbumsUseCase(
        albumRepository: AlbumRepository,
    ) = GetAlbumsUseCase(albumRepository)
}