package com.mazzouzi.feature.album

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.paging.PagingData
import androidx.paging.cachedIn
import androidx.paging.map
import com.mazzouzi.core.domain.model.Album
import com.mazzouzi.core.domain.usecase.GetAlbumsUseCase
import com.mazzouzi.feature.album.mapper.toAlbumUi
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.CoroutineExceptionHandler
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.plus
import timber.log.Timber
import javax.inject.Inject

@HiltViewModel
class AlbumViewModel @Inject constructor(
    useCase: GetAlbumsUseCase
): ViewModel() {

    private val coroutineExceptionHandler = CoroutineExceptionHandler { _, throwable ->
        Timber.e(throwable, "A coroutine exception occured")
    }

    val pagingState = useCase.getAlbums()
        .map { pagingData: PagingData<Album> ->
            pagingData.map { album: Album -> album.toAlbumUi() }
        }
        .cachedIn(viewModelScope + Dispatchers.IO + coroutineExceptionHandler)
}

data class AlbumUi(
    val id: Long,
    val albumId: Long,
    val title: String,
    val url: String,
    val thumbnailUrl: String
)