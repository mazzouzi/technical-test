package com.mazzouzi.core.data.di

import com.mazzouzi.core.data.repository.AlbumPagerFactory
import com.mazzouzi.core.data.repository.AlbumPagerFactoryImpl
import com.mazzouzi.core.data.repository.AlbumRepositoryImpl
import com.mazzouzi.core.domain.repository.AlbumRepository
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ViewModelComponent

@Module
@InstallIn(ViewModelComponent::class)
abstract class DataModule {

    @Binds
    abstract fun bindsAlbumRepository(
        albumRepository: AlbumRepositoryImpl
    ): AlbumRepository

    @Binds
    abstract fun bindsAlbumPagerFactory(
        albumPagerFactory: AlbumPagerFactoryImpl
    ): AlbumPagerFactory
}