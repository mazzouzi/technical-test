package com.mazzouzi.feature.album

import androidx.paging.AsyncPagingDataDiffer
import androidx.paging.PagingData
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListUpdateCallback
import app.cash.turbine.test
import com.mazzouzi.core.domain.model.Album
import com.mazzouzi.core.domain.usecase.GetAlbumsUseCase
import com.mazzouzi.feature.album.mapper.toAlbumUi
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.test.*
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.mockito.Mockito

@ExperimentalCoroutinesApi
class AlbumViewModelTest {

    private lateinit var viewModel: AlbumViewModel
    private val useCase: GetAlbumsUseCase = Mockito.mock(GetAlbumsUseCase::class.java)

    private val testScope = TestScope()
    private val testDispatcher = StandardTestDispatcher(testScope.testScheduler)

    @BeforeEach
    fun setUp() {
        Dispatchers.setMain(testDispatcher)
    }

    @AfterEach
    fun tearDown() {
        Dispatchers.resetMain()
    }

    @Test
    fun `should return mapped albums when getting new albums`() = testScope.runTest {
        val differ = AsyncPagingDataDiffer(
            diffCallback = MyDiffCallback(),
            updateCallback = NoopListCallback(),
            workerDispatcher = Dispatchers.Main
        )

        Mockito.`when`(useCase.getAlbums()).thenReturn(
            flowOf(
                PagingData.from(albums)
            )
        )

        viewModel = AlbumViewModel(useCase)

        viewModel.pagingState.test {
            val data: PagingData<AlbumUi> = awaitItem()
            differ.submitData(data)
            awaitComplete()
        }

        advanceUntilIdle()

        Assertions.assertEquals(
            albums.map { it.toAlbumUi() },
            differ.snapshot().items
        )
    }

    companion object {
        val albums = listOf(
            Album(
                id = 1,
                albumId = 1,
                title = "et quisquam aspernatur",
                url = "https://via.placeholder.com/600/cc12f5",
                thumbnailUrl = "https://via.placeholder.com/150/cc12f5"
            ),
            Album(
                id = 2,
                albumId = 1,
                title = "reprehenderit est deserunt velit ipsam",
                url = "https://via.placeholder.com/600/771796",
                thumbnailUrl = "https://via.placeholder.com/150/771796"
            )
        )
    }
}

class NoopListCallback : ListUpdateCallback {
    override fun onChanged(position: Int, count: Int, payload: Any?) {}
    override fun onMoved(fromPosition: Int, toPosition: Int) {}
    override fun onInserted(position: Int, count: Int) {}
    override fun onRemoved(position: Int, count: Int) {}
}

class MyDiffCallback : DiffUtil.ItemCallback<AlbumUi>() {
    override fun areItemsTheSame(oldItem: AlbumUi, newItem: AlbumUi): Boolean {
        return oldItem == newItem
    }

    override fun areContentsTheSame(oldItem: AlbumUi, newItem: AlbumUi): Boolean {
        return oldItem == newItem
    }
}