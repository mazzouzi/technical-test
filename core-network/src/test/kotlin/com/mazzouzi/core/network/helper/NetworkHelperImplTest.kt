package com.mazzouzi.core.network.helper

import com.mazzouzi.core.network.model.AlbumNetwork
import com.mazzouzi.core.network.response.ApiResponse
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runTest
import okhttp3.ResponseBody.Companion.toResponseBody
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import retrofit2.Response
import java.io.IOException

@OptIn(ExperimentalCoroutinesApi::class)
class NetworkHelperImplTest {

    private lateinit var networkHelper: NetworkHelper

    @BeforeEach
    fun setUp() {
        networkHelper = NetworkHelperImpl()
    }

    @Test
    fun `should return a success when calling network successfully`() = runTest {
        val expected = ApiResponse.Success(albumNetwork)
        val actual = networkHelper.callRemote(
            { Response.success(albumNetwork) },
            null
        )
        Assertions.assertEquals(expected, actual)
    }

    @Test
    fun `should return an error when network call fails`() = runTest {
        val apiResponse: ApiResponse<AlbumNetwork> = networkHelper.callRemote<AlbumNetwork>(
            { Response.error(400, "Bad Request".toResponseBody()) },
            null
        )

        val error = apiResponse as ApiResponse.Error
        Assertions.assertAll(
            { Assertions.assertEquals(400, error.errorCode) },
            { Assertions.assertEquals(null, error.exception) }
        )
    }

    @Test
    fun `should return an exception error when network call throws exception`() = runTest {
        val apiResponse: ApiResponse<Any> = networkHelper.callRemote({ throw IOException() }, null)

        Assertions.assertTrue(apiResponse is ApiResponse.Error)
        val error = apiResponse as ApiResponse.Error
        Assertions.assertEquals(IOException::class.java, error.exception!!::class.java)
    }

    companion object {
        val albumNetwork = listOf(
            AlbumNetwork(
                id = 1,
                albumId = 1,
                title = "et quisquam aspernatur",
                url = "https://via.placeholder.com/600/cc12f5",
                thumbnailUrl = "https://via.placeholder.com/150/cc12f5"
            ),
            AlbumNetwork(
                id = 2,
                albumId = 1,
                title = "reprehenderit est deserunt velit ipsam",
                url = "https://via.placeholder.com/600/771796",
                thumbnailUrl = "https://via.placeholder.com/150/771796"
            )
        )
    }
}