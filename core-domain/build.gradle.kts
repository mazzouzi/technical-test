import com.mazzouzi.build.logic.Deps

plugins {
    id("java-library")
    id("org.jetbrains.kotlin.jvm")
    id("kotlin-kapt") // no longer necessary
    id("com.mazzouzi.dependency")
    id ("com.mazzouzi.unit.testing")
}

java {
    sourceCompatibility = JavaVersion.VERSION_1_8
    targetCompatibility = JavaVersion.VERSION_1_8
}

tasks.test {
    useJUnitPlatform()
}

dependencies {
    implementation(Deps.coroutine)

    // Hilt is no longer necessary for this module. Just keeping it for reference.
    implementation(Deps.hilt_core)
    kapt(Deps.hilt_compiler)

    implementation(Deps.paging_common)
}