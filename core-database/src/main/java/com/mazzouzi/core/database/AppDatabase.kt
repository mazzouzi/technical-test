package com.mazzouzi.core.database

import androidx.room.Database
import androidx.room.RoomDatabase
import com.mazzouzi.core.database.dao.AlbumDao
import com.mazzouzi.core.database.entity.AlbumEntity

@Database(
    entities = [AlbumEntity::class],
    version = 1,
    exportSchema = true
)
abstract class AppDatabase : RoomDatabase() {
    abstract fun albumDao(): AlbumDao
}