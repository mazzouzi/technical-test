package com.mazzouzi.core.common.android.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.paging.LoadState
import androidx.paging.LoadStateAdapter
import androidx.recyclerview.widget.RecyclerView
import com.mazzouzi.core.common.databinding.ItemLoaderStateBinding

class LoaderStatePagingAdapter(
    private val retry: () -> Unit
) : LoadStateAdapter<LoaderStatePagingAdapter.LoaderStateItemViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, loadState: LoadState): LoaderStateItemViewHolder {
        val binding = ItemLoaderStateBinding.inflate(
            LayoutInflater.from(parent.context),
            parent,
            false
        )
        return LoaderStateItemViewHolder(binding, retry)
    }

    override fun onBindViewHolder(holder: LoaderStateItemViewHolder, loadState: LoadState) {
        holder.bind(loadState)
    }

    class LoaderStateItemViewHolder(
        private val binding: ItemLoaderStateBinding,
        private val retry: () -> Unit
    ) : RecyclerView.ViewHolder(binding.root) {

        init {
            binding.retry.setOnClickListener {
                retry()
            }
        }

        fun bind(loadState: LoadState) {
            with(binding) {
                loader.isVisible = loadState is LoadState.Loading
                retry.isVisible = loadState is LoadState.Error
                error.isVisible = loadState is LoadState.Error
            }
        }
    }
}