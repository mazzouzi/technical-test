package com.mazzouzi.build.logic

import com.mazzouzi.build.logic.Version.ARCH_CORE_TESTING_VERSION
import com.mazzouzi.build.logic.Version.ESPRESSO_VERSION
import com.mazzouzi.build.logic.Version.JUNIT_VERSION
import com.mazzouzi.build.logic.Version.TEST_CORE_KTX
import org.gradle.api.Plugin
import org.gradle.api.Project
import org.gradle.kotlin.dsl.dependencies

class InstrumentedTestingPlugin : Plugin<Project> {

    override fun apply(target: Project) {
        with(target) {

            dependencies {
                add("androidTestImplementation", "org.junit.jupiter:junit-jupiter-api:$JUNIT_VERSION")
                add("androidTestRuntimeOnly", "org.junit.jupiter:junit-jupiter-engine:$JUNIT_VERSION")
                add("androidTestImplementation", "org.junit.jupiter:junit-jupiter-params:$JUNIT_VERSION")

                add("androidTestImplementation", "androidx.arch.core:core-testing:$ARCH_CORE_TESTING_VERSION")
                add("androidTestImplementation", "androidx.test.espresso:espresso-core:$ESPRESSO_VERSION")

                add("implementation", "androidx.test:core-ktx:$TEST_CORE_KTX")
            }
        }
    }
}