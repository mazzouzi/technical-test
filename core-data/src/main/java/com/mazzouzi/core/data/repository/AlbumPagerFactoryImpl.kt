package com.mazzouzi.core.data.repository

import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.PagingData
import com.mazzouzi.core.database.entity.AlbumEntity
import com.mazzouzi.core.domain.model.Album
import com.mazzouzi.core.network.model.AlbumNetwork
import com.mazzouzi.core.network.response.ApiResponse
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class AlbumPagerFactoryImpl @Inject constructor() : AlbumPagerFactory {

    override fun getAlbums(
        pageSize: Int,
        prefetchDistance: Int,
        enablePlaceholders: Boolean,
        getLocalData: suspend (Int, Int) -> List<AlbumEntity>,
        getRemoteData: suspend () -> ApiResponse<List<AlbumNetwork>>,
        updateLocalData: suspend (List<AlbumNetwork>) -> Unit,
        needToFetchRemoteAlbums: () -> Boolean,
        resetRemoteSyncTime: () -> Unit,
        mapToAlbum: (List<AlbumEntity>) -> List<Album>
    ): Flow<PagingData<Album>> =
        Pager(
            config = PagingConfig(
                pageSize = pageSize,
                prefetchDistance = prefetchDistance,
                enablePlaceholders = enablePlaceholders
            ),
            pagingSourceFactory = {
                GenericPagingSource(
                    getLocalData,
                    getRemoteData,
                    updateLocalData,
                    needToFetchRemoteAlbums,
                    resetRemoteSyncTime,
                    mapToAlbum
                )
            }
        ).flow
}