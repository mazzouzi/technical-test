package com.mazzouzi.core.common.android

import android.widget.ViewFlipper

/**
 * This interface defines the behavior of a fragment which has to switch the display between a
 * recycler view, a loader and a no result view.
 *
 * A view flipper containing all these view should be defined in the layout.
 *-
 * /!\ The order of the views should be the following one: /!\
 * (0) Loader
 * (1) Content
 * (2) Error view
 *
 */
interface LoadingFlipperInterface {

    val viewFlipper: ViewFlipper?

    fun showLoaderView() {
        viewFlipper?.displayedChild = 0
    }

    fun showContent() {
        viewFlipper?.displayedChild = 1
    }

    fun showErrorView() {
        viewFlipper?.displayedChild = 2
    }
}
