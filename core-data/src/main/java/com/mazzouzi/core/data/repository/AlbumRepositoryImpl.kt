package com.mazzouzi.core.data.repository

import androidx.paging.PagingData
import com.mazzouzi.core.data.mapper.toAlbum
import com.mazzouzi.core.data.mapper.toAlbumEntity
import com.mazzouzi.core.database.dao.AlbumDao
import com.mazzouzi.core.database.entity.AlbumEntity
import com.mazzouzi.core.domain.model.Album
import com.mazzouzi.core.domain.repository.AlbumRepository
import com.mazzouzi.core.network.datasource.AlbumRemoteDataSource
import com.mazzouzi.core.network.model.AlbumNetwork
import dagger.hilt.android.scopes.ViewModelScoped
import kotlinx.coroutines.flow.Flow
import java.util.*
import javax.inject.Inject

@ViewModelScoped
class AlbumRepositoryImpl @Inject constructor(
    private val local: AlbumDao,
    private val remote: AlbumRemoteDataSource,
    private val pagerFactory: AlbumPagerFactory
): AlbumRepository {

    private var remoteSyncTime: Date? = null

    override fun getAlbums(): Flow<PagingData<Album>> = pagerFactory.getAlbums(
        30,
        10,
        false,
        ::getLocalAlbumsByPage,
        ::getRemoteAlbums,
        ::updateLocalAlbums,
        ::needToFetchRemoteAlbums,
        ::resetRemoteSyncTime,
        ::mapToAlbum
    )

    private suspend fun getLocalAlbumsByPage(limit: Int, offset: Int) =
        local.getAlbumEntitiesByPage(limit, offset)

    private suspend fun getRemoteAlbums() = remote.getAlbums()

    /**
     * Sync remote data only if it's the first time or last fetch time is > to 10 seconds
     * in order to avoid spamming servers
     */
    private fun needToFetchRemoteAlbums(): Boolean =
        remoteSyncTime == null ||
                remoteSyncTime!!.before(
                    Calendar.getInstance().apply { add(Calendar.SECOND, -10) }.time
                )

    private fun resetRemoteSyncTime() {
        remoteSyncTime = Date()
    }

    /**
     * Delete local albums that are no longer returned from the API and then update the DB with
     * up-to-date remote data
     */
    private suspend fun updateLocalAlbums(remoteAlbums: List<AlbumNetwork>) {
        val toStore = remoteAlbums.toAlbumEntity()

        local.getAlbumEntities()
            .filter { albumEntity -> albumEntity !in toStore }
            .let { toDelete -> if (toDelete.isNotEmpty()) local.deleteAlbums(toDelete) }
            .also { local.insertOrReplaceAlbumEntities(toStore) }
    }

    private fun mapToAlbum(entities: List<AlbumEntity>) = entities.toAlbum()
}