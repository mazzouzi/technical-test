package com.mazzouzi.core.data.repository

import androidx.paging.PagingSource
import com.mazzouzi.core.data.mapper.toAlbum
import com.mazzouzi.core.data.mapper.toAlbumEntity
import com.mazzouzi.core.database.dao.AlbumDao
import com.mazzouzi.core.database.entity.AlbumEntity
import com.mazzouzi.core.domain.model.Album
import com.mazzouzi.core.network.datasource.AlbumRemoteDataSource
import com.mazzouzi.core.network.model.AlbumNetwork
import com.mazzouzi.core.network.response.ApiResponse
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runTest
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.mockito.BDDMockito.anyInt
import org.mockito.Mockito
import org.mockito.kotlin.argumentCaptor
import java.io.IOException

@ExperimentalCoroutinesApi
class GenericPagingSourceTest {

    private lateinit var genericPagingSource: GenericPagingSource<AlbumNetwork, AlbumEntity, Album>

    private val local: AlbumDao = Mockito.mock(AlbumDao::class.java)
    private val remote: AlbumRemoteDataSource = Mockito.mock(AlbumRemoteDataSource::class.java)

    @BeforeEach
    fun setup() {
        genericPagingSource = GenericPagingSource(
            getLocalData = ::getLocalData,
            getRemoteData = ::getRemoteData,
            updateLocalData = ::updateLocalData,
            needToFetchRemoteData = ::needToFetchRemoteData,
            resetRemoteSyncTime = ::resetRemoteSyncTime,
            mapToDomain = ::mapToDomain
        )
    }

    private suspend fun getLocalData(limit: Int, offset: Int): List<AlbumEntity> =
        local.getAlbumEntitiesByPage(limit, offset)

    private suspend fun getRemoteData() = remote.getAlbums()

    private fun needToFetchRemoteData(): Boolean = true

    private fun needToFetchRemoteDataNot(): Boolean = false

    private fun resetRemoteSyncTime() {}

    private suspend fun updateLocalData(remoteAlbums: List<AlbumNetwork>) {
        local.insertOrReplaceAlbumEntities(remoteAlbums.toAlbumEntity())
    }

    private fun mapToDomain(entities: List<AlbumEntity>) = entities.toAlbum()

    @Test
    fun `should return local albums when network request succeeds and albums exist in DB`() = runTest {
        Mockito.`when`(remote.getAlbums()).thenReturn(ApiResponse.Success(albumNetwork))
        Mockito.`when`(local.getAlbumEntitiesByPage(anyInt(), anyInt())).thenReturn(albumEntities)

        val actualResult = genericPagingSource.load(
            PagingSource.LoadParams.Refresh(
                key = 0,
                loadSize = 30,
                placeholdersEnabled = false
            )
        )

        assertEquals(
            PagingSource.LoadResult.Page(
                albumEntities.toAlbum(),
                null,
                1
            ),
            actualResult
        )
    }

    @Test
    fun `should return local albums when network request fails but albums exist in DB`() = runTest {
        Mockito.`when`(remote.getAlbums()).thenReturn(ApiResponse.Error(IOException(), 400))
        Mockito.`when`(local.getAlbumEntitiesByPage(anyInt(), anyInt())).thenReturn(albumEntities)

        val actualResult = genericPagingSource.load(
            PagingSource.LoadParams.Refresh(
                key = 0,
                loadSize = 30,
                placeholdersEnabled = false
            )
        )

        assertEquals(
            PagingSource.LoadResult.Page(
                albumEntities.toAlbum(),
                null,
                1
            ),
            actualResult
        )
    }

    @Test
    fun `should insert mapped album entities in DB from network albums when loading first page`() = runTest {
        Mockito.`when`(remote.getAlbums()).thenReturn(ApiResponse.Success(albumNetwork))

        genericPagingSource.load(
            PagingSource.LoadParams.Refresh(
                key = 0,
                loadSize = 30,
                placeholdersEnabled = false
            )
        )

        val argumentCaptor = argumentCaptor<List<AlbumEntity>>()
        Mockito.verify(local).insertOrReplaceAlbumEntities(argumentCaptor.capture())
        assertEquals(albumNetwork.toAlbumEntity(), argumentCaptor.firstValue)
    }

    @Test
    fun `should return nextKey equals null when there is no more albums in DB`() = runTest {
        Mockito.`when`(local.getAlbumEntitiesByPage(anyInt(), anyInt())).thenReturn(emptyList())

        val actualResult =  genericPagingSource.load(
            PagingSource.LoadParams.Refresh(
                key = 77,
                loadSize = 30,
                placeholdersEnabled = false
            )
        )

        assertEquals(
            PagingSource.LoadResult.Page(
                data = emptyList(),
                prevKey = null,
                nextKey = null
            ),
            actualResult
        )
    }

    @Test
    fun `should return prevKey equals null and nextKey equals 2 when key equals 1`() = runTest {
        Mockito.`when`(local.getAlbumEntitiesByPage(anyInt(), anyInt())).thenReturn(albumEntities)

        val actualResult =  genericPagingSource.load(
            PagingSource.LoadParams.Refresh(
                key = 1,
                loadSize = 30,
                placeholdersEnabled = false
            )
        )

        assertEquals(
            PagingSource.LoadResult.Page(
                data = albumEntities.toAlbum(),
                prevKey = null,
                nextKey = 2
            ),
            actualResult
        )
    }

    @Test
    fun `should return IllegalStateException when network request fails and no albums exist in DB`() = runTest {
        Mockito.`when`(remote.getAlbums()).thenReturn(ApiResponse.Error(IOException(), 400))
        Mockito.`when`(local.getAlbumEntitiesByPage(anyInt(), anyInt())).thenReturn(emptyList())

        val actualResult =  genericPagingSource.load(
            PagingSource.LoadParams.Refresh(
                key = 0,
                loadSize = 30,
                placeholdersEnabled = false
            )
        )

        assertEquals(
            PagingSource.LoadResult.Error<Int, Album>(IllegalStateException()).toString(),
            actualResult.toString()
        )
    }

    @Test
    fun `should return database exception if any`() = runTest {
        Mockito.`when`(remote.getAlbums()).thenReturn(ApiResponse.Success(albumNetwork))
        Mockito.`when`(local.getAlbumEntitiesByPage(anyInt(), anyInt())).thenThrow(NullPointerException())

        val actualResult = genericPagingSource.load(
            PagingSource.LoadParams.Refresh(
                key = 0,
                loadSize = 30,
                placeholdersEnabled = false
            )
        )

        assertEquals(
            PagingSource.LoadResult.Error<Int, Album>(NullPointerException()).toString(),
            actualResult.toString()
        )
    }

    @Test
    fun `should call remote api when we are loading the first page and remoteSyncTime passed the required delay`(): Unit = runTest {
        genericPagingSource.load(
            PagingSource.LoadParams.Refresh(
                key = 0, // first page
                loadSize = 30,
                placeholdersEnabled = false
            )
        )

        Mockito.verify(remote).getAlbums()
    }

    @Test
    fun `should not call remote api when we are loading the first page and last api call is too recent`(): Unit = runTest {
        genericPagingSource = GenericPagingSource(
            getLocalData = ::getLocalData,
            getRemoteData = ::getRemoteData,
            updateLocalData = ::updateLocalData,
            needToFetchRemoteData = ::needToFetchRemoteDataNot, // return false
            resetRemoteSyncTime = ::resetRemoteSyncTime,
            mapToDomain = ::mapToDomain
        )

        genericPagingSource.load(
            PagingSource.LoadParams.Refresh(
                key = 0, // first page
                loadSize = 30,
                placeholdersEnabled = false
            )
        )

        Mockito.verifyNoInteractions(remote)
    }

    @Test
    fun `should not call remote api when we are not loading the first page`() = runTest {
        genericPagingSource.load(
            PagingSource.LoadParams.Refresh(
                key = 1, // second page
                loadSize = 30,
                placeholdersEnabled = false
            )
        )

        Mockito.verifyNoInteractions(remote)
    }

    companion object {
        val albumNetwork = listOf(
            AlbumNetwork(
                id = 1,
                albumId = 1,
                title = "et quisquam aspernatur",
                url = "https://via.placeholder.com/600/cc12f5",
                thumbnailUrl = "https://via.placeholder.com/150/cc12f5"
            ),
            AlbumNetwork(
                id = 2,
                albumId = 1,
                title = "reprehenderit est deserunt velit ipsam",
                url = "https://via.placeholder.com/600/771796",
                thumbnailUrl = "https://via.placeholder.com/150/771796"
            )
        )

        val albumEntities = albumNetwork.toAlbumEntity()
    }
}