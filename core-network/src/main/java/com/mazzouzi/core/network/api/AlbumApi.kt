package com.mazzouzi.core.network.api

import com.mazzouzi.core.network.model.AlbumNetwork
import retrofit2.Response
import retrofit2.http.GET

interface AlbumApi {

    @GET("/img/shared/technical-test.json")
    suspend fun getAlbums(): Response<List<AlbumNetwork>>
}