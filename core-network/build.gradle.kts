import com.mazzouzi.build.logic.Deps

plugins {
    id("java-library")
    id("org.jetbrains.kotlin.jvm")
    id("kotlin-kapt")
    id("com.mazzouzi.dependency")
    id ("com.mazzouzi.unit.testing")
}

java {
    sourceCompatibility = JavaVersion.VERSION_1_8
    targetCompatibility = JavaVersion.VERSION_1_8
}

tasks.test {
    useJUnitPlatform()
}

dependencies {

    implementation(project(":core-common-kotlin"))

    implementation(Deps.coroutine)

    implementation(Deps.moshi)
    kapt(Deps.moshi_codegen)

    implementation(Deps.retrofit)
    implementation(Deps.retrofit_moshi_converter)

    implementation(Deps.okhttp)
    implementation(Deps.okhttp_logging_interceptor)

    implementation(Deps.hilt_core)
    kapt(Deps.hilt_compiler)
}
