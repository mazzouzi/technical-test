import com.mazzouzi.build.logic.Deps

plugins {
    id("com.android.application")
    id("org.jetbrains.kotlin.android")
    id("kotlin-kapt")
    id("dagger.hilt.android.plugin")
    id("de.mannodermaus.android-junit5")
    id("com.mazzouzi.dependency")
    id("com.jraska.module.graph.assertion") version "2.2.0"
}

android {
    compileSdk = 32

    defaultConfig {
        applicationId = "com.mazzouzi.technicaltest"

        minSdk = 24
        targetSdk = 32
        versionCode = 1
        versionName = "1.0"

        testInstrumentationRunner = "androidx.test.runner.AndroidJUnitRunner"
    }

    buildTypes {
        release {
            isMinifyEnabled = false
            proguardFiles(getDefaultProguardFile("proguard-android-optimize.txt"), "proguard-rules.pro")
        }
    }
    
    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_1_8
        targetCompatibility  = JavaVersion.VERSION_1_8
    }

    kotlinOptions {
        jvmTarget = "1.8"
    }
}

dependencies {

    implementation(project(":feature-album"))
    implementation(project(":core-data"))

    implementation(Deps.app_compat)
    implementation(Deps.constraint_layout)

    implementation(Deps.hilt)
    kapt(Deps.hilt_compiler)

    implementation(Deps.timber)
}