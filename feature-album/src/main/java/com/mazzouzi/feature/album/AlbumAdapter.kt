package com.mazzouzi.feature.album

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import com.mazzouzi.core.common.R.drawable
import com.mazzouzi.feature.album.databinding.ItemAlbumBinding
import dagger.hilt.android.scopes.FragmentScoped
import javax.inject.Inject

@FragmentScoped
class AlbumAdapter @Inject constructor(
) : PagingDataAdapter<AlbumUi, AlbumAdapter.ViewHolder>(DIFF_CALLBACK) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding = ItemAlbumBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(getItem(position))
    }

    class ViewHolder(
        private val binding: ItemAlbumBinding
    ) : RecyclerView.ViewHolder(binding.root) {

        fun bind(item: AlbumUi?) {
            binding.title.text = item?.title
            Glide.with(itemView.context)
                .load(item?.thumbnailUrl)
                .transition(DrawableTransitionOptions.withCrossFade(150))
                .placeholder(drawable.image_loading_placeholder)
                .into(binding.thumbnail)
        }
    }

    companion object {
        val DIFF_CALLBACK = object : DiffUtil.ItemCallback<AlbumUi>() {
            override fun areItemsTheSame(oldItem: AlbumUi, newItem: AlbumUi): Boolean =
                oldItem.id == newItem.id

            override fun areContentsTheSame(oldItem: AlbumUi, newItem: AlbumUi): Boolean =
                oldItem == newItem
        }
    }
}