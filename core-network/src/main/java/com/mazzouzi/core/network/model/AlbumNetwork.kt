package com.mazzouzi.core.network.model

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class AlbumNetwork(
    @field:Json(name = "id") val id: Long,
    @field:Json(name = "albumId") val albumId: Long,
    @field:Json(name = "title") val title: String,
    @field:Json(name = "url") val url: String,
    @field:Json(name = "thumbnailUrl") val thumbnailUrl: String,
)