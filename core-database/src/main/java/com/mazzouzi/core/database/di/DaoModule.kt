package com.mazzouzi.core.database.di

import com.mazzouzi.core.database.AppDatabase
import com.mazzouzi.core.database.dao.AlbumDao
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object DaoModule {

    @Singleton
    @Provides
    fun providesAlbumDao(
        database: AppDatabase,
    ): AlbumDao = database.albumDao()
}