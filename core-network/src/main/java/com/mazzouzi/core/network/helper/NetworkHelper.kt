package com.mazzouzi.core.network.helper

import com.mazzouzi.core.network.response.ApiResponse
import retrofit2.Response

interface NetworkHelper {

    suspend fun <T> callRemote(
        call: suspend () -> Response<T>,
        predicate: ((t: T) -> Boolean)? = null
    ): ApiResponse<T>
}