package com.mazzouzi.core.network.datasource

import com.mazzouzi.core.common.kotlin.AppDispatchers.IO
import com.mazzouzi.core.common.kotlin.Dispatcher
import com.mazzouzi.core.network.api.AlbumApi
import com.mazzouzi.core.network.helper.NetworkHelper
import com.mazzouzi.core.network.helper.NetworkHelperImpl
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.withContext
import javax.inject.Inject

class AlbumRemoteDataSourceImpl @Inject constructor(
    private val remote: AlbumApi,
    @Dispatcher(IO) private val ioDispatcher: CoroutineDispatcher
) : AlbumRemoteDataSource, NetworkHelper by NetworkHelperImpl() {

    override suspend fun getAlbums() = withContext(ioDispatcher) {
        callRemote(remote::getAlbums)
    }
}