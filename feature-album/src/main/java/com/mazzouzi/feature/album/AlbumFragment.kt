package com.mazzouzi.feature.album

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.ViewFlipper
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.flowWithLifecycle
import androidx.lifecycle.lifecycleScope
import androidx.paging.LoadState
import androidx.paging.PagingData
import androidx.recyclerview.widget.DividerItemDecoration
import com.mazzouzi.core.common.android.LoadingFlipperInterface
import com.mazzouzi.core.common.android.adapter.LoaderStatePagingAdapter
import com.mazzouzi.feature.album.databinding.FragmentAlbumBinding
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.flow.distinctUntilChangedBy
import kotlinx.coroutines.launch
import javax.inject.Inject

@AndroidEntryPoint
class AlbumFragment : Fragment(), LoadingFlipperInterface {

    override val viewFlipper: ViewFlipper
        get() = binding.viewFlipper

    private var _binding: FragmentAlbumBinding? = null
    private val binding get() = _binding!!

    private val viewModel: AlbumViewModel by viewModels()

    @Inject lateinit var adapter: AlbumAdapter

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentAlbumBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initViews()

        viewLifecycleOwner.lifecycleScope.launch {
            viewModel.pagingState
                .flowWithLifecycle(viewLifecycleOwner.lifecycle, Lifecycle.State.STARTED)
                .collect { albumUiState: PagingData<AlbumUi> ->
                    adapter.submitData(albumUiState)
                }
        }

        viewLifecycleOwner.lifecycleScope.launchWhenStarted {
            adapter.loadStateFlow
                .distinctUntilChangedBy { it.refresh }
                .collectLatest {
                    when (it.refresh) {
                        is LoadState.NotLoading -> {
                            binding.refresh.isRefreshing = false
                            showContent()
                        }
                        is LoadState.Error -> {
                            showErrorView()
                        }
                        LoadState.Loading -> {
                            if (!binding.refresh.isRefreshing) {
                                showLoaderView()
                            }
                        }
                    }
                }
        }
    }

    private fun initViews() {
        binding.recyclerView.adapter = adapter.withLoadStateFooter(
            footer = LoaderStatePagingAdapter(adapter::retry)
        )
        val dividerItemDecoration = DividerItemDecoration(context, LinearLayout.VERTICAL)
        binding.recyclerView.addItemDecoration(dividerItemDecoration)

        binding.retry.setOnClickListener {
            showLoaderView()
            adapter.retry()
        }

        binding.refresh.setOnRefreshListener {
            adapter.refresh()
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    companion object {
        fun newInstance() = AlbumFragment()
    }
}