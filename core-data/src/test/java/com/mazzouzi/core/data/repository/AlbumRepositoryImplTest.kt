package com.mazzouzi.core.data.repository

import androidx.paging.PagingData
import androidx.paging.map
import app.cash.turbine.test
import com.mazzouzi.core.data.mapper.toAlbum
import com.mazzouzi.core.database.dao.AlbumDao
import com.mazzouzi.core.database.entity.AlbumEntity
import com.mazzouzi.core.domain.model.Album
import com.mazzouzi.core.network.datasource.AlbumRemoteDataSource
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runTest
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.mockito.ArgumentMatchers.anyInt
import org.mockito.Mockito

@OptIn(ExperimentalCoroutinesApi::class)
class AlbumRepositoryImplTest {

    private lateinit var albumRepositoryImpl: AlbumRepositoryImpl

    private val local = Mockito.mock(AlbumDao::class.java)
    private val remote = Mockito.mock(AlbumRemoteDataSource::class.java)
    private val fakePagerFactory = FakeAlbumPagerFactoryImpl()

    @BeforeEach
    fun init() {
        albumRepositoryImpl = AlbumRepositoryImpl(local, remote, fakePagerFactory)
    }

    @Test
    fun `should return first albums when fetching albums from paging source`() = runTest {
        Mockito.`when`(local.getAlbumEntitiesByPage(anyInt(), anyInt())).thenReturn(albumEntities)
        Mockito.`when`(local.getAlbumEntities()).thenReturn(albumEntities)

        val albums = albumEntities.toAlbum()

        albumRepositoryImpl.getAlbums().test {
            val albumPagingData: PagingData<Album> = awaitItem()
            albumPagingData.map { album ->
                Assertions.assertTrue(albums.contains(album))
            }
            awaitComplete()
        }
    }

    companion object {
        val albumEntities = listOf(
            AlbumEntity(
                id = 1,
                albumId = 1,
                title = "et quisquam aspernatur",
                url = "https://via.placeholder.com/600/cc12f5",
                thumbnailUrl = "https://via.placeholder.com/150/cc12f5"
            ),
            AlbumEntity(
                id = 2,
                albumId = 1,
                title = "reprehenderit est deserunt velit ipsam",
                url = "https://via.placeholder.com/600/771796",
                thumbnailUrl = "https://via.placeholder.com/150/771796"
            )
        )
    }
}