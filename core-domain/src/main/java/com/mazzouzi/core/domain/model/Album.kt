package com.mazzouzi.core.domain.model

data class Album(
    val id: Long,
    val albumId: Long,
    val title: String,
    val url: String,
    val thumbnailUrl: String
)
